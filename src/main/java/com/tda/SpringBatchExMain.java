package com.tda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchExMain {

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchExMain.class, args);

	}

}
