package com.tda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tda.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{

}
