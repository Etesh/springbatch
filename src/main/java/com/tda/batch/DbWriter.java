package com.tda.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tda.dao.CustomerRepository;
import com.tda.models.Customer;

@Component
public class DbWriter implements ItemWriter<Customer>{
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public void write(List<? extends Customer> customers) throws Exception {
		
		customerRepository.save(customers);
	}
	
	

}
