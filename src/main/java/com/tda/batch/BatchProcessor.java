package com.tda.batch;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.tda.models.Customer;

@Component
public class BatchProcessor implements  ItemProcessor<Customer, Customer>{
	
	private static final Map<String,String> deptName=new HashMap<String, String>();
	public BatchProcessor() {
		deptName.put("01", "JavaDeveloper");
		deptName.put("02", "PythonDeveloper");
		deptName.put("03", "JavacsriptDeveloper");
	}
	public Customer process(Customer customer) throws Exception{
		String deptCode=customer.getDept();
		String dept=deptName.get(deptCode);
		customer.setDept(dept);
		return customer;
	}

}
